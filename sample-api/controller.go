package sample_api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func getSampledUserCollection() []User {
	test2testUsername, test3testUsername := "test2test", "test3test"

	return []User{
		{
			Fullname: "Test Test Test",
			Id: "e9eca285-80ee-4ab7-8e25-d4673ecbffa1",
		},
		{
			Fullname: "Test2 Test2 Test2",
			Id: "91f427e9-cf45-4720-82b8-be368cdd063b",
			Username: &test2testUsername,
		},
		{
			Fullname: "Test3 Test3 Test3",
			Id: "620ef55c-446e-400c-94c5-9dc13426522d",
			Username: &test3testUsername,
		},
		{
			Fullname: "Test4 Test4 Test4",
			Id: "88091743-4b88-415b-aaf3-b505b600bacc",
		},
	}
}

func findUserInSampledCollection(id string) (User, bool) {
	res := User{}
	found := false

	users := getSampledUserCollection()
	for i := range users {
		if users[i].Id == id {
			res = users[i]
			found = true
			break
		}
	}

	return res, found
}

type Controller struct {}

func NewController() *Controller {
	return &Controller{}
}

// This function wraps sending of an error in the Error format, and
// handling the failure to marshal that.
func sendApiError(w http.ResponseWriter, code int, message string) {
	apiErr := Error{
		Code:    int32(code),
		Message: message,
	}
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(apiErr)
}

func (c *Controller) ListUsers(w http.ResponseWriter, r *http.Request, params ListUsersParams) {
	result := getSampledUserCollection()

	if params.Limit != nil {
		l := int(*params.Limit)
		if len(result) >= l {
			result = result[:l]
		}
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)
}

func (c *Controller) GetUserById(w http.ResponseWriter, r *http.Request, id string) {
	res, found := findUserInSampledCollection(id)
	if !found {
		sendApiError(w, http.StatusNotFound, fmt.Sprintf("Could not find user with ID %d", id))
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(res)
}