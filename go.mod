module bitbucket.org/perfectgentlemande/go-openapi-example

go 1.13

require (
	github.com/deepmap/oapi-codegen v1.5.1
	github.com/go-chi/chi v4.1.2+incompatible
)
