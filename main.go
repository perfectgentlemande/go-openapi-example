package main

import (
	sample_api "bitbucket.org/perfectgentlemande/go-openapi-example/sample-api"
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-chi/chi"
)

func main() {
	ctx := context.Background()

	ctrl := sample_api.NewController()
	r := chi.NewRouter()

	sample_api.HandlerFromMux(ctrl, r)

	srv := &http.Server{
		Addr:    ":3333",
		Handler: r,
	}

	fmt.Printf("Starting server on %v\n", srv.Addr)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("start server: ", err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	stop := <-done
	log.Println("signal", stop.String())

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
}